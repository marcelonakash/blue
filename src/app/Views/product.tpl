<div class="container">
    <div class="row">
        <div class="col-12">
            <h2><?php echo $name ?></h2>
        </div>
        <div class="col-6">
            <img src="<?php echo $image ?>" width="500" />
        </div>
        <div class="col-6">
            <p><?php echo $description ?></p>
            <h3 class="text-success"><?php echo $price ?></h3>
            <label>Quantidade </label>
            <div >
                <button class="btn btn-success add float-left mr-1">+</button>
                <input type="number" class="form-control col-2 float-left" id="quantity" value="1" disabled/>
                <button class="btn btn-danger sub ml-1">-</button>
            </div>
            <button class="btn btn-success float-right add-carrinho" id="<?php echo $id ?>">Adicionar ao carrinho</button>
            <div class="alert alert-success cart-message float-left mt-3 col-12 text-center" style="display: none;">Produto adicionado ao carrinho</div>
        </div>

        <h3>Caracteristicas</h3>
        <hr />
        <div class="table-responsive">
            <table class="table table-striped">
                <?php foreach($characteristics as $charac): ?>
                    <tr>
                        <td><?php echo $charac['name'] ?></td>
                        <td><?php echo $charac['value'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>

<script>
    var Product = {
        init: function ()
        {
            this.observerAdd();
            this.observerSub();
            this.observerAddCarrinho();
        },
        observerAddCarrinho: function() {
            $('.add-carrinho').click(function () {
                var id = $(this).attr('id');
                var quantity = $('#quantity').val();

                $('.add-carrinho').attr('disabled','disabled');

                $.post('/cart',{id: id, quantity: quantity},function (res) {
                    $('.carrinho-items').text(res.items);
                    $('.add-carrinho').removeAttr('disabled','disabled');
                    $('.cart-message').css('display','block');
                    setTimeout(function (){$('.cart-message').css('display','none');},1000)
                },'json');
            });
        },
        observerAdd: function ()
        {
            $('.add').click(function () {
                var quantity = $('#quantity').val();
                $('#quantity').val(++quantity);
            });
        },
        observerSub: function ()
        {
            $('.sub').click(function () {
                var quantity = $('#quantity').val();
                if(quantity > 1)
                    $('#quantity').val(--quantity);
            });
        },
    };
    $(document).ready(function (){
        Product.init();
    });
</script>