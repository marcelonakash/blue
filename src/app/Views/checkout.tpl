<div class="container">
    <h2>Confirmação de Pedido</h2>
    <hr />

    <h4>Dados do comprador</h4>
    <br />
    <form id="pedido" method="post" action="/quote">
        <div class="form-row">
            <div class="col">
                <label>Nome</label>
                <input class="form-control name" value="<?php echo $name ?>" disabled>
            </div>
            <div class="col">
                <label>Email</label>
                <input class="form-control name" value="<?php echo $email ?>" disabled>
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <label>CPF</label>
                <input class="form-control document" name="document" value="<?php echo $document ?>">
            </div>
        </div>
        <br />
        <h4>Endereço de entrega</h4>
        <br />
        <div class="form-row">
            <div class="col">
                <label>CEP</label>
                <input class="form-control zipcode" name="zipcode" value="<?php echo $zipcode ?>">
            </div>
            <div class="col">
                <label>Endereço</label>
                <input class="form-control address" name="address" value="<?php echo $address ?>">
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <label>Número</label>
                <input class="form-control address_number" name="address_number" value="<?php echo $address_number ?>">
            </div>
            <div class="col">
                <label>Bairro</label>
                <input class="form-control neighborhood" name="neighborhood" value="<?php echo $neighborhood ?>">
            </div>
        </div>

        <div class="form-row">
            <div class="col">
                <label>Complemento</label>
                <input class="form-control complement" name="complement" value="<?php echo $complement ?>">
            </div>
            <div class="col">
                <label>Cidade</label>
                <input class="form-control city" name="city" value="<?php echo $city ?>">
            </div>
        </div>


        <div class="form-row">
            <div class="col-6">
                <label>Estado</label>
                <input class="form-control state" maxlength="2" name="state" value="<?php echo $state ?>">
            </div>
        </div>
    <br />
    <h3>Itens do pedido</h3>
    <br />
      <div class="table-responsive">
        <table class="table">
          <thead>
            <tr>
                <th></th>
                <th>Produto</th>
                <th>Preço</th>
                <th>Quantidade</th>
                <th>Total</th>
            </tr>
          </thead>
          <tbody>
                <?php foreach($products as $product): ?>
                <tr id="<?php echo $product['id'] ?>">
                    <td><img src="<?php echo $product['image'] ?>" width="100" /></td>
                    <td><?php echo $product['name'] ?></td>
                    <td><?php echo $product['price'] ?></td>
                    <td><?php echo $product['quantity'] ?></td>
                    <td class="total"><?php echo $product['total'] ?></td>
                </tr>
                <?php endforeach; ?>
          </tbody>
          <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="final-total"><?php echo $total ?></td>
                <td></td>
            </tr>
          </tfoot>
        </table>
      </div>
        <div class="alert alert-danger" style="display: none;"></div>
      <button class="btn btn-primary float-right" type="submit">Confirmar Pedido</button>
    </form>
</div> <!-- /container -->
<script>
    var Checkout = {
        init: function ()
        {
            $('.zipcode').mask('00000-000');
            $('.document').mask('000.000.000-00');
            this.observerZipcode();
            this.observerPedido();
        },
        observerPedido: function () {
            $('#pedido').submit(function (e) {

                $('.alert-danger').css('display','none');

                var message = [];

                if($('.document').val() == '')
                    message.push("informe o CPF");
                if($('.zipcode').val() == '')
                    message.push("informe o CEP");
                if($('.address').val() == '')
                    message.push("informe o enderço");
                if($('.address_number').val() == '')
                    message.push("informe o número");
                if($('.neighborhood').val() == '')
                    message.push("informe o bairro");
                if($('.city').val() == '')
                        message.push("informe a cidade");
                if($('.state').val() == '')
                        message.push("informe o estado");

                if(message.length > 0){
                    $('.alert-danger').html(message.join('<br />'));
                    $('.alert-danger').css('display','block');
                    e.preventDefault();
                }
            });
        },
        observerZipcode: function() {
            $('.zipcode').blur(function (){
                var zipcode = $(this).val().replace(/\D/g, '');
                $.getJSON("https://viacep.com.br/ws/"+ zipcode +"/json/?callback=?", function(res) {
                    $(".address").val(res.logradouro);
                    $(".neighborhood").val(res.bairro);
                    $(".city").val(res.localidade);
                    $(".state").val(res.uf);
                });
            });
        }
    };
    $(document).ready(function (){
        Checkout.init();
    });
</script>