<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Ecommerce</title>
    <meta name="author" content="">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/jquery-ui.min.css" />
    <link href="/css/style.css" rel="stylesheet" />

    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
</head>

  <body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="/">Ecommerce</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown active">
            <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorias</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
                <?php foreach ($categories as $category) : ?>
                  <a class="dropdown-item" href="/home?category=<?php echo $category->id; ?>"><?php echo $category->name; ?></a>
                <?php endforeach; ?>
            </div>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0 mr-lg-2" action="/home">
          <input class="form-control mr-sm-2" type="text" name="search" id="search" placeholder="Pesquisar" aria-label="Pesquisar">
          <button class="btn btn-sm btn-outline-success" type="submit">Pesquisar</button>
        </form>
        <a href="/cart" class="btn btn-outline-light btn-sm mr-lg-2">Carrinho <span class="badge badge-light carrinho-items"><?php echo $itens ?></span></a>
        <ul class="navbar-nav ml-2">
          <?php if ($user): ?>
            <li class="nav-item dropdown active user-dropdown">
              <a class="nav-link dropdown-toggle" id="dropdown01" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user->name ?></a>
              <div class="dropdown-menu" aria-labelledby="dropdown01">
                <a class="dropdown-item" href="/quote">Meus Pedidos</a>
                <a class="dropdown-item" href="/account/logout">Logout</a>
              </div>
            </li>
          <?php else: ?>
            <li class="nav-item active">
              <a class="nav-link" href="/account">Login</a>
            </li>
          <?php endif; ?>
        </ul>
      </div>
    </nav>

    <main role="main">
        <?php include $view ?>
    </main>
    <script>
      $(document).ready(function () {
          $.widget( "custom.productcomplete", $.ui.autocomplete, {
              _create: function() {
                this._super();
                this.widget().menu( "option", "items", "> :not(.ui-autocomplete-product)" );
              },
              _renderMenu: function( ul, items ) {
                var that = this,
                  currentProduct = "";
                $.each( items, function( index, item ) {
                  var li = that._renderItemData( ul, item );
                  if ( item.name ) {
                    li.find('div').append('<img src="'+item.image+'" width="30"/>'+item.name);
                  }
                });
              }
            });
          $( "#search" ).productcomplete({
            source: "/home/search",
            minLength: 2,
            select: function( event, ui ) {
              window.location.href = '/products/'+ui.item.id;
            }
          });
      });
    </script>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
  </body>
</html>
