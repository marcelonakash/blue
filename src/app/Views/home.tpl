 <div class="container">
<!-- Example row of columns -->
<h2><?php echo $categoryName ?></h2>

<div class="row">
    <?php if($products): ?>
        <?php foreach($products as $product): ?>
            <div class="col-md-4 text-center">
              <a href="/products/<?php echo $product['id'] ?>" class="text-decoration-none">
                <img src="<?php echo $product['image'] ?>" width="150" />
                <h3 class="text-secondary"><?php echo $product['name'] ?></h3>
              </a>
                <h4 class="text-success"><?php echo $product['price'] ?></h4>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="alert alert-danger col-md-12">Nenhum produto encontrado.</div>
    <?php endif; ?>
</div>
</div> <!-- /container -->
