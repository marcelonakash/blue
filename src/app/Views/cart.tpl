
<div class="container">
  <!-- Example row of columns -->
    <?php if($products): ?>
          <div class="table-responsive">
          <table class="table">
              <thead>
                <tr>
                    <th></th>
                    <th>Produto</th>
                    <th>Preço</th>
                    <th>Quantidade</th>
                    <th>Total</th>
                </tr>
              </thead>
              <tbody>
                 <?php foreach($products as $product): ?>
                    <tr id="<?php echo $product['id'] ?>">
                        <td><img src="<?php echo $product['image'] ?>" width="100" /></td>
                        <td style="width: 30%;"><?php echo $product['name'] ?></td>
                        <td><span class="price"><?php echo $product['price'] ?></span></td>
                        <td>
                            <button class="btn btn-success btn-sm add">+</button>
                            <input type="number" class="quantity col-md-4" value="<?php echo $product['quantity'] ?>" disabled/>
                            <button class="btn btn-danger btn-sm sub">-</button>
                        </td>
                        <td class="total"><?php echo $product['total'] ?></td>
                        <td><button class="btn btn-danger btn-sm remove">Remover</button></td>
                    </tr>
                  <?php endforeach; ?>
              </tbody>
              <tfoot>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="final-total"><?php echo $total ?></td>
                    <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
          <a href="/checkout" class="btn btn-primary float-right">Finalizar Compra</a>
    <?php else: ?>
          <div class="alert alert-danger col-md-12">Carrinho vazio.</div>
      <?php endif; ?>
</div> <!-- /container -->
<script>
    var Cart = {
        init: function ()
        {
            this.observerAdd();
            this.observerSub();
            this.observerRemove();
        },
        observerAdd: function ()
        {
            var self = this;
            $('.add').click(function () {
                var quantity = $(this).parent().find('.quantity').val();
                $(this).parent().find('.quantity').val(++quantity);
                self.updateCart($(this).parent().find('.quantity'));
            });
        },
        observerSub: function ()
        {
            var self = this;
            $('.sub').click(function () {
                var quantity = $(this).parent().find('.quantity').val();
                if(quantity > 1)
                    $(this).parent().find('.quantity').val(--quantity);

                self.updateCart($(this).parent().find('.quantity'));
            });
        },
        observerRemove: function ()
        {
            var self = this;
            $('.remove').click(function () {
                var id = $(this).parent().parent().attr('id');
                $(this).parent().parent().remove();
                $.ajax({
                    url: 'cart/'+id,
                    type: 'DELETE',
                    success: function (res) {
                        $('.carrinho-items').text(res.items);
                        self.recalculateCart();
                    }
                });
            })
        },
        updateCart: function (obj)
        {
            $.ajax({
                    url:'/cart',
                    type: 'PUT',
                    dataType:'json',
                    data: JSON.stringify({id: $(obj).parent().parent().attr('id'), quantity: obj.val()}),
                    success: function (res) {
                        $('.carrinho-items').text(res.items);
                    }
                });

            this.recalculateCart();
        },
        recalculateCart: function ()
        {
            var total = 0;
            $('.table tbody tr').each(function (index,item) {
                var price = $(item).find('.price').text();
                var quantity = $(item).find('.quantity').val();
                price = price.replace('R$ ','').replace('.','').replace(',','.');
                price = parseFloat(price);
                total += price * quantity;
                $(item).find('.total').text((price * quantity).toLocaleString('pt-BR',{ style: 'currency', currency: 'BRL' }));
            });

            $('.final-total').text(total.toLocaleString('pt-BR',{ style: 'currency', currency: 'BRL' }));

        }
    };
    $(document).ready(function (){
        Cart.init();
    });
</script>