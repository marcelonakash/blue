<div class="container">
  <!-- Example row of columns -->
      <?php if ($quotes): ?>
          <div class="list-group">
              <?php foreach($quotes as $quote): ?>
                  <a href="javascript:void(0)" class="list-group-item list-group-item-action">
                      <small class="float-right"><?php echo $quote->created_at ?></small>
                      <p class="mb-1 font-weight-bold">
                          Endereço de entrega: <?php echo $quote->address .', '.$quote->address_number .' - '.$quote->neighborhood
                            .' - '.$quote->city .' - '.$quote->state .' - CEP: '.$quote->zipcode.' '.$quote->complement ?>
                      </p>
                      <br />
                      <?php foreach($quote->details as $detail): ?>
                        <div class="row">
                            <div class="col-8">
                                <p><img src="<?php echo $detail->product['image'] ?>" width="30"/><?php echo $detail->product['name'] ?></p>
                            </div>
                            <div class="col-4">
                                <small class="float-right">Quantidade: <?php echo $detail->quantity; ?></small>
                                <small class="float-right mr-3">Preço Uni: <?php echo 'R$ '.number_format($detail->price,2,',','.'); ?></small>
                            </div>
                        </div>
                      <?php endforeach; ?>
                      <h6 class="float-right">Total <?php echo 'R$ '.number_format($quote->total,2,',','.'); ?></h6>
                    </a>
              <?php endforeach; ?>
          </div>
    <?php else: ?>
          <div class="alert alert-danger col-md-12">Nenhum pedido encontrado.</div>
      <?php endif; ?>
</div>