<div class="container">
<div class="row">
    <div class="col-6">
        <h2 class="text-secondary">Cadastre-se</h2>
        <form id="registrar">
            <div class="alert alert-danger" style="display: none;"></div>
            <div class="form-group">
                <label>Nome</label>
                <input name="name" class="form-control name" required/>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input name="email" type="email" class="form-control email" required/>
            </div>
            <div class="form-group">
                <label>Senha</label>
                <input name="password" type="password" class="form-control password" required/>
            </div>
            <div class="form-group">
                <label>Confirmação de senha</label>
                <input name="repassword" type="password" class="form-control repassword" required/>
            </div>
            <button class="btn btn-primary float-right" type="submit">Registrar-se</button>
        </form>
    </div>
    <div class="col-6">
        <h2 class="text-secondary">Realizar login</h2>
        <form id="logar">
            <div class="alert alert-danger" style="display: none;"></div>
            <div class="form-group">
                <label>Email</label>
                <input name="email" type="email" class="form-control email" required/>
            </div>
            <div class="form-group">
                <label>Senha</label>
                <input name="password" type="password" class="form-control password" required/>
            </div>
            <button class="btn btn-info float-right" type="submit">Login</button>
        </form>
    </div>
</div>
</div>
<script>
    var Account = {
        init: function ()
        {
            this.observerRegistrar();
            this.observerLogin();
        },
        observerLogin: function ()
        {
            $('#logar').submit(function (e) {
                e.preventDefault();

                $('#logar .alert').css('display','none');

                var message = [];

                if($('#logar .email').val().trim() == '') {
                    message.push("Informe o email");
                }

                if($('#logar .password').val() == '') {
                    message.push("Informe a senha");
                }

                if(message.length > 0) {
                    $('#logar .alert').css('display','block');
                    $('#logar .alert').html(message.join('<br />'));
                    return;
                }

                $.post('account/login',{
                    email: $('#logar .email').val(),
                    password: $('#logar .password').val()
                },function (res) {
                    if(res.message) {
                        $('#logar .alert').css('display','block');
                        $('#logar .alert').html(res.message);
                        return;
                    }
                    window.location.href = '/checkout';
                },'json').fail(function (res) {
                    console.log(res);
                    $('#logar .alert').css('display','block');
                    $('#logar .alert').html(res.responseJSON.message);
                });

            });
        },
        observerRegistrar: function ()
        {
            $('#registrar').submit(function (e) {
                e.preventDefault();

                $('#registrar .alert').css('display','none');

                var message = [];
                if($('#registrar .name').val().trim() == '') {
                    message.push("Informe o nome");
                }

                if($('#registrar .email').val().trim() == '') {
                    message.push("Informe o email");
                }

                if($('#registrar .password').val() == '') {
                    message.push("Informe a senha");
                }

                if($('#registrar .password').val().trim() != '' && $('#registrar .password').val() != $('#registrar .repassword').val()) {
                    message.push("Confirmação de senha não confere");
                }

                if(message.length > 0) {
                    $('#registrar .alert').css('display','block');
                    $('#registrar .alert').html(message.join('<br />'));
                    return;
                }

                $.post('account/create',{
                    name: $('#registrar .name').val(),
                    email: $('#registrar .email').val(),
                    password: $('#registrar .password').val()
                },function (res) {
                    window.location.href = res.cameFrom ? res.cameFrom : '/checkout';
                },'json').fail(function (res) {
                    $('#registrar .alert').css('display','block');
                    $('#registrar .alert').html(res.responseJSON.message);
                });
            })
        }
    };

    $(document).ready(function () {
        Account.init();
    });
</script>