<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 17:01
 */

namespace Nakashima\Controllers;


use Nakashima\Builders\QuoteBuilder;
use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Framework\Session;
use Nakashima\Services\QuoteService;
use Nakashima\Framework\Validate;

class QuoteController extends Controller
{
    private QuoteService $quoteService;
    private QuoteBuilder $quoteBuilder;

    public function __construct(QuoteBuilder $quoteBuilder,QuoteService $quoteService)
    {
        $this->quoteService = $quoteService;
        $this->quoteBuilder = $quoteBuilder;
    }

    public function index(Request $request, Response $response)
    {
        $this->protectedRoute();
        $this->loadDefaulViewParamenters($response);
        $quotes = $this->quoteService->all(Session::get('user')->id);
        return $response->view('quotes', ['quotes' => $quotes]);
    }

    public function create(Request $request,Response $response)
    {
        Validate::validate(
            $request, [
            'document' => 'required',
            'address' => 'required',
            'address_number' => 'required',
            'neighborhood' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required'
            ]
        );

        $this->protectedRoute();

        $quote = $this->quoteBuilder->build($request);
        $this->quoteService->create($quote);
        Session::set('cart', null);
        Session::set('items', null);
        $response->redirect('/quote');
    }
}