<?php

namespace Nakashima\Controllers;

use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Services\CategoriesService;
use Nakashima\Services\ProductsService;
use Nakashima\Transformer\ProductsTransformer;

class HomeController extends Controller
{
    private ProductsService $productsService;
    private CategoriesService $categoriesService;
    public function __construct(ProductsService $productsService, CategoriesService $categoriesService)
    {
        $this->productsService = $productsService;
        $this->categoriesService = $categoriesService;
    }

    public function index(Request $request, Response $response)
    {
        $categoryName = '';
        $this->loadDefaulViewParamenters($response);

        $products = $this->productsService->search($request->getParam('category'), $request->getParam('search'));
        $products = ProductsTransformer::transform($products);

        if($request->getParam('category')) {
            $category = $this->categoriesService->get($request->getParam('category'));
            if($category) {
                $categoryName = $category->name;
            }
        }

        return $response->view('home', ['products' => $products,'categoryName' => $categoryName]);
    }

    public function search(Request $request, Response $response)
    {
        return $response->json(
            ProductsTransformer::transform(
                $this->productsService->search($request->getParam('category'), $request->getParam('term'))
            )
        );
    }
}
