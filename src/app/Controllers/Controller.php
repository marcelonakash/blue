<?php

namespace Nakashima\Controllers;

use Nakashima\Framework\Container;
use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Framework\Session;
use Nakashima\Services\CategoriesService;

class Controller
{
    public function loadDefaulViewParamenters(Response $response)
    {
        $categoriesServices = Container::get(CategoriesService::class);
        $numItens = Session::get('items') ?? 0;
        $response->appendViewParameter('user', Session::get('user'));
        $response->appendViewParameter('itens', $numItens);
        $response->appendViewParameter('categories', $categoriesServices->all());
    }

    public function protectedRoute()
    {
        if(!Session::get('user')) {
            header('location: /account');
        }
    }
}
