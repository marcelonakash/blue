<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 07:01
 */

namespace Nakashima\Controllers;


use Nakashima\Builders\QuoteBuilder;
use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Framework\Session;
use Nakashima\Services\CartService;

class CheckoutController extends Controller
{
    private CartService $cartService;
    private QuoteBuilder $quoteBuilder;

    public function __construct(CartService $cartService,QuoteBuilder $quoteBuilder)
    {
        $this->cartService = $cartService;
        $this->quoteBuilder = $quoteBuilder;
    }

    public function index(Request $request, Response $response)
    {
        $this->loadDefaulViewParamenters($response);

        $user = Session::get('user');

        if(empty($user)) {
            Session::set('cameFrom', '/checkout');
            $response->redirect('/account');
        }

        list($products,$total) = $this->cartService->getProducts();

        if(empty($products)) {
            $response->redirect('/');
        }

        $total = 'R$ '. number_format($total, 2, ',', '.');

        return $response->view('checkout', array_merge((array) $user, ['products' => $products,'total' => $total]));
    }
}