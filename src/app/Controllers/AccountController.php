<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 07:02
 */

namespace Nakashima\Controllers;


use Nakashima\Entities\User;
use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Framework\Session;
use Nakashima\Framework\Validate;
use Nakashima\Services\AccountService;
use LogicException;

class AccountController extends Controller
{
    private AccountService $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    public function index(Request $request, Response $response)
    {
        $this->loadDefaulViewParamenters($response);

        return $response->view('account', []);
    }

    public function create(Request $request, Response $response)
    {
        Validate::validate(
            $request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
            ]
        );

        $user = new User();
        $user->name = $request->getParam('name');
        $user->email = $request->getParam('email');
        $user->password = $request->getParam('password');

        try {
            $user = $this->accountService->create($user);
        } catch (LogicException $e) {
            return $response->json(['message' => $e->getMessage()], 422);
        }
        Session::set('user', $user);

        return $response->json((array) $user);
    }

    public function login(Request $request, Response $response)
    {
        $user = new User();
        $user->email = $request->getParam('email');
        $user->password = $request->getParam('password');
        $user = $this->accountService->login($user);

        if(!$user) {
            return $response->json(['message' => 'Usuário ou senha inválido'], 422);
        }

        Session::set('user', $user);

        return $response->json(['cameFrom' => Session::get('cameFrom')]);
    }

    public function logout(Request $request, Response $response)
    {
        Session::destroy();
        $response->redirect('/');
    }
}