<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 05:04
 */

namespace Nakashima\Controllers;


use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Framework\Session;
use Nakashima\Framework\Validate;
use Nakashima\Services\CartService;

class CartController extends Controller
{
    private CartService $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index(Request $request,Response $response)
    {
        $this->loadDefaulViewParamenters($response);

        list($products,$total) = $this->cartService->getProducts();
        $total = 'R$ '. number_format($total, 2, ',', '.');
        return $response->view('cart', ['products' => $products,'total' => $total]);
    }

    public function add(Request $request, Response $response)
    {
        Validate::validate(
            $request, [
            'id' => 'required|numeric',
            'quantity' => 'required|numeric'
            ]
        );

        $this->cartService->add($request->getParam('id'), $request->getParam('quantity'));

        return $response->json(['items' => Session::get('items')]);
    }

    public function update(Request $request, Response $response)
    {
        Validate::validate(
            $request, [
            'id' => 'required|numeric',
            'quantity' => 'required|numeric'
            ]
        );

        $cart = $this->cartService->update($request->getParam('id'), $request->getParam('quantity'));
        return $response->json(['items' => array_sum($cart)]);
    }

    public function delete($id, Request $request, Response $response)
    {
        $items = $this->cartService->delete($id);
        return $response->json(['items' => $items]);
    }
}