<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 03:15
 */

namespace Nakashima\Controllers;

use Nakashima\Exceptions\HttpException;
use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Services\ProductsService;
use Nakashima\Transformer\ProductsTransformer;

class ProductController extends Controller
{
    private ProductsService $productsService;

    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }

    public function show(int $id,Request $request, Response $response)
    {
        $this->loadDefaulViewParamenters($response);
        $product = $this->productsService->get($id);

        if(!$product) {
            throw new HttpException("Produto não encontrado", 404);
        }

        return $response->view('product', ProductsTransformer::transform($product)[0]);
    }
}