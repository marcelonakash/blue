<?php

namespace Nakashima\Framework;

use Throwable;

class Response
{
    private array $viewParams = [];

    public function appendViewParameter(string $key,$value)
    {
        $this->viewParams[$key] = $value;
    }

    public function view(string $view, array $params)
    {
        return View::parse($view, array_merge($this->viewParams, $params));
    }

    public function redirect($url)
    {
        header("location: $url");
    }

    public function json(array $json, $code = 200)
    {
        http_response_code($code);
        header('content-type: application/json');
        echo json_encode($json);
        exit;
    }
}
