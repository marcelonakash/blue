<?php

namespace Nakashima\Framework;

use Nakashima\Config\Config;
use PDO;

class Database
{
    private static $instance;
    public PDO $conn;

    private function __construct()
    {
        $config = new Config();

            $dsn = sprintf('mysql:host=%s;dbname=%s;port=3306;charset=utf8mb4;', $config->dbHost, $config->dbDatabase);

            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            ];

            $this->conn = new PDO($dsn, $config->dbUsername, $config->dbPassword, $options);
    }

    public static function getInstance(): Database
    {
        if(is_null(self::$instance)) {
            self::$instance = new Database();
        }

        return self::$instance;
    }
}
