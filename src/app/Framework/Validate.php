<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 21/06/20
 * Time: 17:09
 */

namespace Nakashima\Framework;

use Nakashima\Exceptions\HttpException;

class Validate
{
    public static function validate(Request $request,array $params)
    {
        foreach($params as $field => $rules)
        {
            $rules = explode("|", $rules);
            foreach($rules as $rule)
            {
                switch ($rule) {
                case 'required':
                    if(empty($request->getParam($field))) {
                        throw new HttpException("$field is required", 442);
                    }
                    break;
                case 'numeric':
                    if(!is_numeric($request->getParam($field))) {
                        throw new HttpException("$field must be numeric", 422);
                    }
                }
            }
        }
    }
}