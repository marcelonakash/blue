<?php

namespace Nakashima\Framework;

use Nakashima\Exceptions\HttpException;
use Nakashima\Framework\Request;
use Nakashima\Framework\Response;
use Nakashima\Framework\Container;

class Router
{
    protected $routes = [];

    public function get(string $pattern, $class, $method = 'index'): void
    {
        $this->routes[] = ['GET', $pattern, $class, $method];
    }

    public function put(string $pattern, $class, $method = 'update'): void
    {
        $this->routes[] = ['PUT', $pattern, $class, $method];
    }

    public function post(string $pattern, $class, $method = 'create'): void
    {
        $this->routes[] = ['POST', $pattern, $class, $method];
    }

    public function patch(string $pattern, $class, $method = 'edit'): void
    {
        $this->routes[] = ['PATCH', $pattern, $class, $method];
    }

    public function delete(string $pattern, $class, $method = 'delete'): void
    {
        $this->routes[] = ['DELETE', $pattern, $class, $method];
    }

    public function match(string $requestMethod, string $requestPath)
    {
        $found = false;
        $request = new Request($_GET, $_POST, $_SERVER, 'php://input');
        $response = new Response();

        foreach ($this->routes as $route) {
            list($verb, $pattern, $class, $method) = $route;

            if ($verb != '' && $requestMethod != $verb) {
                continue;
            }

            // extract parameter values from URL
            $values = [];
            if (!preg_match('#^' . $pattern . '(\?|$)#', $requestPath, $values)) {
                continue;
            }
            array_shift($values);
            $values = array_filter(
                $values, function ($item) {
                    return $item != '?' && !empty($item);
                }
            );
            $instance = Container::get($class);
            if($values) {
                return call_user_func_array([$instance,$method], array_merge($values, [$request,$response]));
            } else { 
                return call_user_func_array([$instance,$method], [$request,$response]);
            }

            $found = true;
            break;
        }

        if(!$found) {
            throw new HttpException("Url não encontrada", 404);
        }
    }
}
