<?php

namespace Nakashima\Framework;

class Request
{
    protected array $get;
    protected array $post;
    protected array $server;
    protected array $input;

    public function __construct(array $get, array $post,array $server, $input)
    {
        // trim request data
        $this->get = filter_var($get, FILTER_CALLBACK, ['options' => 'trim']);
        $this->post = filter_var($post, FILTER_CALLBACK, ['options' => 'trim']);
        $this->server = $server;
        if (!is_array($input)) {
            $input = (array) json_decode((string) file_get_contents($input), true, 10);
        }
        $this->input = filter_var($input, FILTER_CALLBACK, ['options' => 'trim']);
    }

    public function getParam(string $key)
    {
        return $this->input[$key] ?? $this->post[$key] ?? $this->get[$key] ?? '';
    }

}
