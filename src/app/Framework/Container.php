<?php

namespace Nakashima\Framework;

use ReflectionClass;

class Container
{
    public static function get(string $class): Object
    {
        $refClass = new ReflectionClass($class);
        $constructor = $refClass->getConstructor();
        if(!$constructor) {
            return new $class;
        }

        $params = $constructor->getParameters();
        if(!$params) {
            return new $class;
        }

        $constructorParams = [];
        foreach($params as $key => $param) {
            if($param->getClass()) {
                $constructorParams[] = self::get($param->getClass()->name);
            }
        }
        $reflection = new ReflectionClass($class);

        return $reflection->newInstanceArgs($constructorParams);
    }
}
