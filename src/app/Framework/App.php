<?php

namespace Nakashima\Framework;

use Nakashima\Exceptions\HttpException;
use Nakashima\Router\Router;

class App
{
    public function run()
    {
        try{
            echo (new Router())->run();
        } catch (HttpException $e) {
            if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
                (new Response())->json(['error' => $e->getMessage()], $e->getCode());
                return;
            }
            http_response_code($e->getCode());
            echo $e->getMessage();
        } catch (\Throwable $e) {
            http_response_code(500);
            echo "Ocorreu um erro inesperado";
        }
    }
}
