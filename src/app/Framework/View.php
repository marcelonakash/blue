<?php

namespace Nakashima\Framework;

class View
{
    public static function parse(string $view, array $params): string
    {
        if(!is_file(__DIR__.'/../Views/'.$view.".tpl")) {
            throw new Exception('View not found');
        }

        $view = __DIR__.'/../Views/'.$view.".tpl";
        extract($params);

        ob_start();
        include __DIR__.'/../Views/layout/main.tpl';
        return ob_get_clean();

        $view = str_replace('{view}', file_get_contents(__DIR__.'/../Views/'.$view.".tpl"), $main);
        $view = self::parseIf($view, $params);
        $view = self::parseForeach($view, $params);
        $view = self::parseSimpleVariable($view, $params);

        return $view;
    }
}