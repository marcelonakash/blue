<?php

namespace Nakashima\Framework;

use Nakashima\Framework\Database;
use PDO;

class Repository
{
    protected string $pk = 'id';
    protected string $table;
    protected string $entity;

    public function find(int $id): ?Object
    {
        $statement = Database::getInstance()->conn->prepare("select * from $this->table where $this->pk = ?");
        $statement->execute([$id]);
        return $statement->fetchObject($this->entity) ?: null;
    }

    public function where(array $params): ?Object
    {
        $where = array_map(
            function ($field) {
                return " $field = ? ";
            }, array_keys($params)
        );

        $statement = Database::getInstance()->conn->prepare("select * from $this->table where ".implode(' and ', $where)." limit 1");
        $statement->execute(array_values($params));
        return $statement->fetchObject($this->entity) ?: null;
    }

    public function whereAll(array $params): array
    {
        $where = array_map(
            function ($field) {
                return " $field = ? ";
            }, array_keys($params)
        );

        $statement = Database::getInstance()->conn->prepare("select * from $this->table where ".implode(' and ', $where));
        $statement->execute(array_values($params));
        return $statement->fetchAll(PDO::FETCH_CLASS, $this->entity);
    }

    public function all(): array
    {
        $statement = Database::getInstance()->conn->prepare("select * from $this->table order by created_at desc");
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS, $this->entity);
    }
}
