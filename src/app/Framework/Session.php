<?php

namespace Nakashima\Framework;

class Session
{
    static function get($key) 
    {
        return $_SESSION[$key] ?? null;
    }

    static function set($key,$value) 
    {
        $_SESSION[$key] = $value;
    }

    static function destroy()
    {
        unset($_SESSION);
        session_unset();
        session_destroy();
    }
}