<?php

namespace Nakashima\Config;

class Config
{
    public string $dbHost = 'mysql';
    public string $dbUsername = 'nakashima';
    public string $dbPassword = 'nakashima';
    public string $dbDatabase = 'app';
}
