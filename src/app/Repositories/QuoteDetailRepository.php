<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 17:30
 */

namespace Nakashima\Repositories;


use Nakashima\Entities\QuoteDetail;
use Nakashima\Framework\Repository;

class QuoteDetailRepository extends Repository
{
    protected string $table = 'quote_details';
    protected string $entity = QuoteDetail::class;
}