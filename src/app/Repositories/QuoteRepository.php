<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 17:02
 */

namespace Nakashima\Repositories;


use Nakashima\Entities\Quote;
use Nakashima\Framework\Database;
use Nakashima\Framework\Repository;

class QuoteRepository extends Repository
{
    protected string $table = 'quotes';
    protected string $entity = Quote::class;

    public function create(Quote $quote): Quote
    {
        $statement = Database::getInstance()->conn->prepare(
            "
            insert into quotes (
                total,
                user_id,
                document,
                address,
                address_number,
                neighborhood,
                complement,
                city,
                state,
                zipcode
            ) values (?,?,?,?,?,?,?,?,?,?)
        "
        );

        $statement->execute(
            [
            $quote->total,
            $quote->user_id,
            $quote->document,
            $quote->address,
            $quote->address_number,
            $quote->neighborhood,
            $quote->complement,
            $quote->city,
            $quote->state,
            $quote->zipcode
            ]
        );

        $statement->closeCursor();

        $quoteId = Database::getInstance()->conn->lastInsertId();

        foreach($quote->details as $detail) {
            $statement = Database::getInstance()->conn->prepare(
                "
                        insert into quote_details (
                          quote_id,
                          product_id,
                          price,
                          quantity
                        ) values (?,?,?,?)
                    "
            );
            $statement->execute([$quoteId,$detail->product_id,$detail->price,$detail->quantity]);
            $statement->closeCursor();
        }

        return $this->find($quoteId);
    }
}