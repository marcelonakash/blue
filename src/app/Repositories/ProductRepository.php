<?php

namespace Nakashima\Repositories;

use Nakashima\Entities\Product;
use Nakashima\Framework\Repository;
use Nakashima\Framework\Database;
use PDO;

class ProductRepository extends Repository
{
    protected string $table = 'products';
    protected string $entity = Product::class;

    public function search($category = null, $search = null): array
    {
        $where = [];
        $where[] = $category ? "product_categories.category_id = '$category' " : null;
        $where[] = $search ? "products.name like '%$search%'" : null;
        $where = array_filter($where);
        $where = $where ? " where " . implode(' and ', $where) : '';

        $joinCategory = $category ? ' inner join product_categories on product_categories.product_id = products.id ' : '';

        $statement = Database::getInstance()->conn->prepare(
            "
          select distinct products.* from 
            $this->table 
            $joinCategory
          $where"
        );

        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_CLASS, $this->entity);

    }
}