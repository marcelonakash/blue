<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 07:46
 */

namespace Nakashima\Repositories;

use Nakashima\Entities\User;
use Nakashima\Framework\Database;
use Nakashima\Framework\Repository;

class UserRepository extends Repository
{
    protected string $table = 'users';
    protected string $entity = User::class;

    public function login(User $user): ?User
    {
        return $this->where(['email' => $user->email,'password' => md5($user->password)]);
    }

    public function create(User $user): User
    {
        $statement = Database::getInstance()->conn->prepare(
            "
            insert into users (name,email,password) values(?,?,?)
        "
        );

        $statement->execute([$user->name,$user->email,md5($user->password)]);

        return $this->find(Database::getInstance()->conn->lastInsertId());
    }

    public function update(User $user)
    {
        $statement = Database::getInstance()->conn->prepare(
            "
                    update users set 
                        document=?,
                        address=?,
                        address_number=?,
                        neighborhood=?,
                        complement=?,
                        city=?,
                        state=?,
                        zipcode=?
                    where id = ?
                    "
        );

                $statement->execute(
                    [
                    $user->document,
                    $user->address,
                    $user->address_number,
                    $user->neighborhood,
                    $user->complement,
                    $user->city,
                    $user->state,
                    $user->zipcode,
                    $user->id
                    ]
                );
    }
}