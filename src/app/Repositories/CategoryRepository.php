<?php

namespace Nakashima\Repositories;

use Nakashima\Entities\Category;
use Nakashima\Framework\Repository;

class CategoryRepository extends Repository
{
    protected string $table = 'categories';
    protected string $entity = Category::class;
}
