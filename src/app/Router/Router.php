<?php

namespace Nakashima\Router;

use Nakashima\Controllers\AccountController;
use Nakashima\Controllers\CartController;
use Nakashima\Controllers\CheckoutController;
use Nakashima\Controllers\ProductController;
use Nakashima\Controllers\QuoteController;
use Nakashima\Framework\Router as BaseRouter;
use Nakashima\Controllers\HomeController;

class Router
{
    public function run()
    {
        $router = new BaseRouter();
        $router->get('/', HomeController::class);
        $router->get('/home', HomeController::class);
        $router->get('/home/search', HomeController::class, 'search');
        $router->get('/products/(\d+)', ProductController::class, 'show');
        $router->get('/cart', CartController::class);
        $router->post('/cart', CartController::class, 'add');
        $router->put('/cart', CartController::class, 'update');
        $router->delete('/cart/(\d+)', CartController::class, 'delete');
        $router->get('/checkout', CheckoutController::class);
        $router->get('/quote', QuoteController::class);
        $router->post('/quote', QuoteController::class);
        $router->get('/account', AccountController::class);
        $router->post('/account/create', AccountController::class, 'create');
        $router->post('/account/login', AccountController::class, 'login');
        $router->get('/account/logout', AccountController::class, 'logout');

        return $router->match($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
    }
}
