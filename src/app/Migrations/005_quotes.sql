create table quotes (
  id int not null auto_increment,
  total decimal not null,
  user_id int not null,
  document varchar(14) not null,
  address varchar(150) not null,
  address_number varchar(10) not null,
  neighborhood varchar(150) not null,
  complement varchar(150) null,
  city varchar(150) not null,
  state varchar(2) not null,
  zipcode varchar(9) not null,
  created_at timestamp default now(),
  primary key(id),
  FOREIGN KEY (user_id) REFERENCES users (id)
)