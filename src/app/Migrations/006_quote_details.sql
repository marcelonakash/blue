create table quote_details (
  id int not null auto_increment,
  quote_id int not null,
  product_id int not null,
  price decimal not null,
  quantity int not null,
  created_at timestamp default now(),
  primary key(id),
  FOREIGN KEY (quote_id) REFERENCES quotes (id),
  FOREIGN KEY (product_id) REFERENCES products (id)
)