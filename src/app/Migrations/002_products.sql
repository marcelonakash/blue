create table products (
  id int not null auto_increment,
  name varchar(255) not null,
  description text not null,
  image varchar(255) not null,
  price decimal not null,
  characteristics json not null,
  primary key(id)
)