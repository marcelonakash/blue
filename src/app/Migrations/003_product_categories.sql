create table product_categories (
  product_id int not null,
  category_id int not null,
  FOREIGN KEY (product_id) REFERENCES products (id),
  FOREIGN KEY (category_id) REFERENCES categories (id)
)