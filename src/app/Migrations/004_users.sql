create table users (
  id int not null auto_increment,
  name varchar(255) not null,
  email varchar(150) not null,
  password varchar(150) not null,
  document varchar(14) null,
  address varchar(150) null,
  address_number varchar(10) null,
  neighborhood varchar(150) null,
  complement varchar(150) null,
  city varchar(150) null,
  state varchar(2) null,
  zipcode varchar(9) null,
  primary key(id),
  unique (email)
)
