create table categories (
  id int not null auto_increment,
  name varchar(255) not null,
  created_at timestamp default now(),
  primary key(id)
)