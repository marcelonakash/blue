<?php

namespace Nakashima\Transformer;

class ProductsTransformer
{
    public static function transform($products)
    {
        $return = [];

        if(is_object($products)) {
            $products = [$products];
        }

        foreach($products as $product) {
            $product = (array) $product;
            $product['image'] = '/images/'.$product['image'];
            $product['orig_price'] = $product['price'];
            $product['price'] = 'R$ ' . number_format($product['price'], 2, ',', '.');
            $product['description'] = $product['description'];
            $product['characteristics'] = json_decode($product['characteristics'], true);

            $return[] = $product;
        }

        return $return;
    }
}