<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 17:01
 */

namespace Nakashima\Services;


use Nakashima\Entities\Quote;
use Nakashima\Entities\User;
use Nakashima\Framework\Session;
use Nakashima\Repositories\ProductRepository;
use Nakashima\Repositories\QuoteDetailRepository;
use Nakashima\Repositories\QuoteRepository;
use Nakashima\Repositories\UserRepository;
use Nakashima\Transformer\ProductsTransformer;

class QuoteService
{
    private QuoteRepository $quoteRepository;
    private QuoteDetailRepository $quoteDetailRepository;
    private UserRepository $userRepository;
    private ProductRepository $productRepository;

    public function __construct(
        QuoteRepository $quoteRepository,
        QuoteDetailRepository $quoteDetailRepository,
        UserRepository $userRepository,
        ProductRepository $productRepository
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->quoteDetailRepository = $quoteDetailRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
    }

    public function create(Quote $quote): Quote
    {
        $user = new User();
        $user->id = $quote->user_id;
        $user->document = $quote->document;
        $user->address = $quote->address;
        $user->address_number = $quote->address_number;
        $user->neighborhood = $quote->neighborhood;
        $user->city = $quote->city;
        $user->state = $quote->state;
        $user->zipcode = $quote->zipcode;
        $user->complement = $quote->complement;

        $this->userRepository->update($user);
        Session::set('user', $this->userRepository->find($user->id));
        return $this->quoteRepository->create($quote);
    }

    public function all(int $id)
    {
        $quotes = $this->quoteRepository->whereAll(['user_id' => $id]);
        foreach($quotes as $key => $quote) {
            $quote->created_at = date_format(date_create($quote->created_at), 'd/m/Y H:i:s');
            $quotes[$key]->details = $this->quoteDetailRepository->whereAll(['quote_id' => $quote->id]);

            foreach($quotes[$key]->details as $i => $detail) {
                $quotes[$key]->details[$i]->product = ProductsTransformer::transform(
                    $this->productRepository->find($detail->product_id)
                )[0];
            }
        }
        return $quotes;
    }
}