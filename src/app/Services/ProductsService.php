<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 00:50
 */

namespace Nakashima\Services;


use Nakashima\Entities\Product;
use Nakashima\Repositories\ProductRepository;

class ProductsService
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function get(int $id): ?Product
    {
        return $this->productRepository->find($id);
    }

    public function search($category = null,$search = null): array
    {
        return $this->productRepository->search($category, $search);
    }
}