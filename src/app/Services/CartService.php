<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 08:23
 */

namespace Nakashima\Services;


use Nakashima\Framework\Session;
use Nakashima\Transformer\ProductsTransformer;

class CartService
{
    private ProductsService $productsService;

    public function __construct(ProductsService $productsService)
    {
        $this->productsService = $productsService;
    }

    public function getProducts()
    {
        $cart = Session::get('cart') ?? [];

        if(!$cart) {
            return [null,null];
        }

        $products = [];
        $productIds = array_keys($cart);
        foreach($productIds as $productId) {
            $product = ProductsTransformer::transform($this->productsService->get($productId))[0];
            $product['quantity'] = $cart[$productId];
            $product['total'] = 'R$ '.number_format($product['quantity'] * $product['orig_price'], 2, ',', '.');
            $products[] = $product;
        }

        $total = array_reduce(
            $products,
            function ($carry,$product) {
                $carry += $product['quantity'] * $product['orig_price'];
                return $carry;
            },
            0
        );

        return [$products,$total];
    }

    public function add(int $id, int $quantity)
    {
        $cart = Session::get('cart') ?? [];

        $cart[$id] = $cart[$id] ?? 0;

        $cart[$id] += $quantity;
        Session::set('cart', $cart);
        Session::set('items', array_sum($cart));
    }

    public function update(int $id, int $quatity)
    {
        $cart = Session::get('cart') ?? [];
        $cart[$id] = $cart[$id] ?? 0;
        $cart[$id] = $quatity;
        Session::set('cart', $cart);
        Session::set('items', array_sum($cart));

        return $cart;
    }

    public function delete(int $id)
    {
        $cart =Session::get('cart');
        unset($cart[$id]);
        Session::set('items', array_sum($cart));
        Session::set('cart', $cart);

        return array_sum($cart);
    }
}