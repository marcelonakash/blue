<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 00:47
 */

namespace Nakashima\Services;


use Nakashima\Entities\Category;
use Nakashima\Repositories\CategoryRepository;

class CategoriesService
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function get(int $id): ?Category
    {
        return $this->categoryRepository->find($id);
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }
}