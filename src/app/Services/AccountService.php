<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 07:43
 */

namespace Nakashima\Services;

use Nakashima\Entities\User;
use Nakashima\Repositories\UserRepository;
use LogicException;

class AccountService
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(User $user): User
    {
        $existingUser = $this->userRepository->where(['email' => $user->email]);

        if($existingUser) {
            throw new LogicException("Email já cadastrado");
        }

        return $this->userRepository->create($user);
    }

    public function login(User $user): ?User
    {
        return $this->userRepository->login($user);
    }
}