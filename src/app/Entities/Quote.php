<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 16:47
 */

namespace Nakashima\Entities;


class Quote
{
    public int $id;
    public float $total;
    public int $user_id;
    public string $document;
    public string $address;
    public string $address_number;
    public string $neighborhood;
    public string $complement;
    public string $city;
    public string $state;
    public string $zipcode;
    public string $created_at;
    public array $details;
}