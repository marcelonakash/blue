<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 16:57
 */

namespace Nakashima\Entities;


class QuoteDetail
{
    public int $id;
    public int $product_id;
    public float $price;
    public int $quantity;
}