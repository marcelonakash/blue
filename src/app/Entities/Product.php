<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 00:51
 */

namespace Nakashima\Entities;

class Product
{
    public int $id;
    public string $name;
    public string $description;
    public string $image;
    public float $price;
    public string $characteristics;
}