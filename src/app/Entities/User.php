<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 07:44
 */

namespace Nakashima\Entities;


class User
{
    public int $id;
    public string $name;
    public string $email;
    public string $password;
    public ?string $document;
    public ?string $address;
    public ?string $address_number;
    public ?string $neighborhood;
    public ?string $complement;
    public ?string $city;
    public ?string $state;
    public ?string $zipcode;
}