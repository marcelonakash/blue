<?php
/**
 * Created by PhpStorm.
 * User: nakashima
 * Date: 20/06/20
 * Time: 16:47
 */

namespace Nakashima\Builders;


use Nakashima\Entities\Quote;
use Nakashima\Entities\QuoteDetail;
use Nakashima\Framework\Request;
use Nakashima\Framework\Session;
use Nakashima\Services\CartService;

class QuoteBuilder
{
    private CartService $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function build(Request $request)
    {
        $quote = new Quote();

        list($products, $total) = $this->cartService->getProducts();
        $cart = Session::get('cart');

        $quote->total = $total;
        $quote->user_id = Session::get('user')->id;
        $quote->document = $request->getParam('document');
        $quote->address = $request->getParam('address');
        $quote->address_number = $request->getParam('address_number');
        $quote->neighborhood = $request->getParam('neighborhood');
        $quote->complement = $request->getParam('complement');
        $quote->city = $request->getParam('city');
        $quote->state = $request->getParam('state');
        $quote->zipcode = $request->getParam('zipcode');

        $details = [];

        foreach($products as $product) {
            $detail = new QuoteDetail();
            $detail->product_id = $product['id'];
            $detail->price = $product['orig_price'];
            $detail->quantity = $product['quantity'];
            $details[] = $detail;
        }

        $quote->details = $details;

        return $quote;
    }
}