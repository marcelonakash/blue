# Intruções

Para rodar o projeto tenha o docker instalado e no diretório raiz do projeto rode os seguintes comandos:
```bash
docker-compose up -d
docker exec php-apache composer dumpautoload
docker-compose logs -f mysql # aguarde o log [Server] /usr/sbin/mysqld: ready for connections, após a criação do usuário nakashima para rodar a migracao
docker exec php-apache php console migrate
```
Acesse http://localhost:8080 em seu navegador
